// CB app settings
cb.settings_choices = [
    {name: 'knightMinTip', label: 'Minimum tip to join the Round Table', type: 'int', minValue: 1, defaultValue: 1500},
    {name: 'knightAnnounce', label: 'Text to show when someone become a Knight, the text MEMBERNAME will be replaced with the username of the new member (Note: graphics don\'t work in this text)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: 'MEMBERNAME is a Knight from the Round Table now ! Congratulations !'},
    {name: 'knightMemberList', label: 'List of current Knights, separated by commas (and they need to be the CB username exactly)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: ''},
    {name: 'knightText', label: 'Text to put in front of messages of Knights (e.g. :emoticons)', type: 'str', minLength: 0, maxLength: 30, required: false, defaultValue:''},

    {name: 'scribeAnnounce', label: 'Text to show when someone become a Scribe, the text MEMBERNAME will be replaced with the username of the new member (Note: graphics don\'t work in this text)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: 'MEMBERNAME is a Scribe now ! Congratulations !'},
    {name: 'scribeMemberList', label: 'List of current Scribes, separated by commas (and they need to be the CB username exactly)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: ''},
    {name: 'scribeText', label: 'Text to put in front of messagesof Scribes (e.g. :emoticons)', type: 'str', minLength: 0, maxLength: 30, required: false, defaultValue:''},

    {name: 'royalGuardMinTip', label: 'Minimum tip to be Royal Guard', type: 'int', minValue: 1, defaultValue: 2500},
    {name: 'royalGuardAnnounce', label: 'Text to show when someone become a Royal Guard, the text MEMBERNAME will be replaced with the username of the new member (Note: graphics don\'t work in this text)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: 'MEMBERNAME is a Royal Guard now ! Congratulations !'},
    {name: 'royalGuardMemberList', label: 'List of current Royal Guards, separated by commas (and they need to be the CB username exactly)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: ''},
    {name: 'royalGuardText', label: 'Text to put in front of messages of Royal Guards (e.g. :emoticons)', type: 'str', minLength: 0, maxLength: 30, required: false, defaultValue:''},

    {name: 'kqhAnnounce', label: 'Text to show when someone become a King of the Queen\'s heart, the text MEMBERNAME will be replaced with the username of the new member (Note: graphics don\'t work in this text)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: 'MEMBERNAME is a King of the Queen\'s heart now ! Congratulations !'},
    {name: 'kqhMemberList', label: 'List of current Kings of the Queen\'s heart, separated by commas (and they need to be the CB username exactly)', type: 'str', minLength: 0, maxLength: 10240, required: false, defaultValue: ''},
    {name: 'kqhText', label: 'Text to put in front of messages of Kings of the Queen\'s heart (e.g. :emoticons)', type: 'str', minLength: 0, maxLength: 30, required: false, defaultValue:''},

    {name: 'memberNicknameList', label: 'List of members\' nicknames, in this form : MEMBERNAME1:NICKNAME1, MEMBERNAME2:NICKNAME2...', type: 'str', minLength: 0, maxLength: 20480, required: false, defaultValue:''},

    {name: 'questGraalNumber', label: 'Number of orgasms until the graal', type: 'int', minValue: 1, defaultValue: 100},
    {name: 'currentOrgasmNumber', label: 'Current number of orgasms', type: 'int', minValue: 0, defaultValue: 0},
];

var knightMemberList = {};
var scribeMemberList = {};
var royalGuardMemberList = {};
var kqhMemberList = {};
var memberNicknameList = {};
var commandsList = [];
var currentOrgasmNumber = 0;
var questGraalNumber = 0;

var knightNotice  = "Tip " + cb.settings.knightMinTip + " to become a Knight from the Round Table !";

var graalNotice  = "Congratulation everyone ! You find the graal !";
var showUntilGraalNotice = "NUMBER orgasm(s) left until you find the graal !"; //NUMBER will be replace by number of orgasm util the graal.

// Ranks ids :
var SCRIBE = 'scribe';
var KNIGHT = 'knight';
var ROYAL_GUARD = 'royal Guard';
var KQH = 'kqh';

function showKnightNotice() {
    cb.chatNotice(knightNotice);
    cb.setTimeout(showKnightNotice, 900000);
}

cb.onMessage(function (msg) {
    // user who send the message
    var user = msg['user'];
    var nickname = getNickname(user);

    // Don't process commands and hide them
    if (isCommand(msg['m'])) {
        processCommand(msg);
        msg['X-Spam'] = true;
        return msg;
    }

    if(nickname){
        msg['m'] = '| ' + nickname + ' | : ' + msg['m'];
    }

    if (isKqh(user)) {
        msg['m'] = cb.settings.kqhText + " " + msg['m'];
    } else if (isRoyalGuard(user)) {
        msg['m'] = cb.settings.royalGuardText + " " + msg['m'];
    } else if (isKnight(user)) {
        msg['m'] = cb.settings.knightText + " " + msg['m'];
    } else if (isScribe(user)) {
        msg['m'] = cb.settings.scribeText + " " + msg['m'];
    }

    return msg;
});

cb.onTip(function (tip) {
    var amountTipped = parseInt(tip['amount']);

    if (amountTipped >= cb.settings.royalGuardMinTip && !isScribe(tip['from_user']) && !isRoyalGuard(tip['from_user']) && !isKqh(tip['from_user'])) {
        var announcement = cb.settings.royalGuardAnnounce.replace("MEMBERNAME", tip['from_user']);
        makeRoyalGuard(tip['from_user']);
        cb.chatNotice(announcement);
    } else if (amountTipped >= cb.settings.knightMinTip && !isKnight(tip['from_user']) && !isScribe(tip['from_user']) && !isRoyalGuard(tip['from_user']) && !isKqh(tip['from_user'])) {
        var announcement = cb.settings.knightAnnounce.replace("MEMBERNAME", tip['from_user']);
        makeKnight(tip['from_user']);
        cb.chatNotice(announcement);
    }
});

function isKnight(username) {
    return (username in knightMemberList);
}

function isScribe(username) {
    return (username in scribeMemberList);
}

function isRoyalGuard(username) {
    return (username in royalGuardMemberList);
}

function isKqh(username) {
    return (username in kqhMemberList);
}

function makeKnight(username) {
    knightMemberList[username] = {'u': 1};
}

function makeScribe(username) {
    scribeMemberList[username] = {'u': 1};
}

function makeRoyalGuard(username) {
    royalGuardMemberList[username] = {'u': 1};
}

function makeKqh(username) {
    kqhMemberList[username] = {'u': 1};
}

function removeKnight(username) {
    if (isKnight(username)) {
        delete knightMemberList[username];
    }
}

function removeScribe(username) {
    if (isScribe(username)) {
        delete scribeMemberList[username];
    }
}

function removeRoyalGuard(username) {
    if (isRoyalGuard(username)) {
        delete royalGuardMemberList[username];
    }
}

function removeKqh(username) {
    if (isKqh(username)) {
        delete kqhMemberList[username];
    }
}

function setNickname(username, nickname) {
    if (isScribe(username) || isKnight(username) || isRoyalGuard(username) || isKqh(username)) {
        memberNicknameList[username] = nickname;
    }
}

function removeNickname(username) {
    delete memberNicknameList[username];
}

function getNickname(username) {
    return memberNicknameList[username];
}

function setQuestGraalNumber(number) {
    questGraalNumber = Number(number);
    if (!testFindGraal()) {
        showUntilGraal();
    }
}

function setOrgasmNumber(number) {
    currentOrgasmNumber = Number(number);
    if (!testFindGraal()) {
        showUntilGraal();
    }
}

function addOrgasmNumber(number) {
    currentOrgasmNumber += Number(number);
    if (!testFindGraal()) {
        showUntilGraal();
    }
}

function showUntilGraal() {
    var untilGraal = Number(questGraalNumber) - currentOrgasmNumber;
    cb.chatNotice(showUntilGraalNotice.replace('NUMBER', untilGraal));
}

function testFindGraal(){
    if (currentOrgasmNumber >= Number(questGraalNumber)) {
        cb.chatNotice(graalNotice);
        return true;
    }
    return false;
}

function grabSettings() {
    cb.log("starting grabbing settings");

    if (cb.settings.knightMemberList) {
        var tabKnightMemberSetting = cb.settings.knightMemberList.split(',');
        for(var i = 0 ; i < tabKnightMemberSetting.length ; i++) {
            var clean = tabKnightMemberSetting[i].toLowerCase().replace(/ /g, "");
            makeKnight(clean);
        }
    }

    if (cb.settings.scribeMemberList) {
        var tabScribMemberSetting = cb.settings.scribeMemberList.split(',');
        for(var i = 0 ; i < tabScribMemberSetting.length ; i++) {
            var clean = tabScribMemberSetting[i].toLowerCase().replace(/ /g, "");
            makeScribe(clean);
        }
    }

    if (cb.settings.royalGuardMemberList) {
        var tabRoyalGuardMemberSetting = cb.settings.royalGuardMemberList.split(',');
        for(var i = 0 ; i < tabRoyalGuardMemberSetting.length ; i++) {
            var clean = tabRoyalGuardMemberSetting[i].toLowerCase().replace(/ /g, "");
            makeRoyalGuard(clean);
        }
    }

    if (cb.settings.kqhMemberList) {
        var tabKqhMemberSetting = cb.settings.kqhMemberList.split(',');
        for(var i = 0 ; i < tabKqhMemberSetting.length ; i++) {
            var clean = tabKqhMemberSetting[i].toLowerCase().replace(/ /g, "");
            makeKqh(clean);
        }
    }

    if (cb.settings.memberNicknameList) {
        var tabMemberNicknameSetting = cb.settings.memberNicknameList.split(',');
        for(var i = 0 ; i < tabMemberNicknameSetting.length ; i++) {
            var memberNicknameTuple = tabMemberNicknameSetting[i].split(':');
            var cleanMember = memberNicknameTuple[0].toLowerCase().replace(/ /g, "");
            var nicknameClean = memberNicknameTuple[1].replace(/^\s*/g, "");
            nicknameClean = nicknameClean.replace(/\s*$/g, "");
            setNickname(cleanMember, nicknameClean);
        }
    }

    if (cb.settings.currentOrgasmNumber) {
        currentOrgasmNumber = cb.settings.currentOrgasmNumber;
    }

    if (cb.settings.questGraalNumber) {
        questGraalNumber = cb.settings.questGraalNumber;
    }

    cb.log("finished grabbing settings");
}

function setCommands(){
    var makeScribeCommand = {regex: /^\/makescribe\s+(\w+[^\s])\s*$/, isMod : true, callback: commandMakeScribe};
    var makeKnightCommand = {regex: /^\/makeknight\s+(\w+[^\s])\s*$/, isMod : true, callback: commandMakeKnight};
    var makeRoyalGuardCommand = {regex: /^\/makeroyalguard\s+(\w+[^\s])\s*$/, isMod : true, callback: commandMakeRoyalGuard};
    var makeKqhCommand = {regex: /^\/makekqh\s+(\w+[^\s])\s*$/, isMod : true, callback: commandMakeKqh};

    var removeScribeCommand = {regex: /^\/removescribe\s+(\w+[^\s])\s*$/, isMod : true, callback: commandRemoveScribe};
    var removeKnightCommand = {regex: /^\/removeknight\s+(\w+[^\s])\s*$/, isMod : true, callback: commandRemoveKnight};
    var removeRoyalGuardCommand = {regex: /^\/removeroyalguard\s+(\w+[^\s])\s*$/, isMod : true, callback: commandRemoveRoyalGuard};
    var removeKqhCommand = {regex: /^\/removekqh\s+(\w+[^\s])\s*$/, numberArgs: 1, isMod : true, callback: commandRemoveKqh};

    var setNicknameCommand = {regex: /^\/setnickname\s+(\w+)\s+(.+[^\s])\s*$/, isMod : false, accesser: [SCRIBE], callback: commandSetNickname};
    var removeNicknameCommand = {regex: /^\/removenickname\s+(\w+[^\s])\s*$/, isMod : false, accesser: [SCRIBE], callback: commandRemoveNickname};

    var setCurrentOrgasmNumberCommand = {regex: /^\/setorgasmnumber\s+(\d+)\s*$/, isMod : false, accesser: [SCRIBE] , callback: commandSetOrgasmNumber};
    var addOrgasmCommand = {regex: /^\/addorgasm\s+(\d+)\s*$/, isMod : false, accesser: [SCRIBE], callback: commandAddOrgasmNumber};
    var showUntilGraalCommand = {regex: /^\/showuntilgraal\s*$/, isMod : false, accesser: [SCRIBE, KNIGHT, ROYAL_GUARD, KQH], callback: commandShowUntilGraal};
    var setQuestGraalNumberCommand = {regex: /^\/setquestgraalnumber\s+(\d+)\s*$/, isMod : true, callback: commandSetQuestGraalNumber};

    commandsList.push(makeScribeCommand);
    commandsList.push(makeKnightCommand);
    commandsList.push(makeRoyalGuardCommand);
    commandsList.push(makeKqhCommand);

    commandsList.push(removeScribeCommand);
    commandsList.push(removeKnightCommand);
    commandsList.push(removeRoyalGuardCommand);
    commandsList.push(removeKqhCommand);

    commandsList.push(setNicknameCommand);
    commandsList.push(removeNicknameCommand);

    commandsList.push(setCurrentOrgasmNumberCommand);
    commandsList.push(addOrgasmCommand);
    commandsList.push(showUntilGraalCommand);
    commandsList.push(setQuestGraalNumberCommand);
}

function commandMakeScribe(args) {
    makeScribe(args[0]);
    cb.chatNotice(cb.settings.scribeAnnounce.replace("MEMBERNAME", args[0]));
}

function commandMakeKnight(args) {
    makeKnight(args[0]);
    cb.chatNotice(cb.settings.knightAnnounce.replace("MEMBERNAME", args[0]));
}

function commandMakeRoyalGuard(args) {
    makeRoyalGuard(args[0]);
    cb.chatNotice(cb.settings.royalGuardAnnounce.replace("MEMBERNAME", args[0]));
}

function commandMakeKqh(args) {
    makeKqh(args[0]);
    cb.chatNotice(cb.settings.kqhAnnounce.replace("MEMBERNAME", args[0]));
}

function commandRemoveScribe(args) {
    removeScribe(args[0]);
}

function commandRemoveKnight(args) {
    removeKnight(args[0]);
}

function commandRemoveRoyalGuard(args) {
    removeRoyalGuard(args[0]);
}

function commandRemoveKqh(args) {
    removeKqh(args[0]);
}

function commandSetNickname(args) {
    setNickname(args[0], args[1]);
}

function commandRemoveNickname(args) {
    removeNickname(args[0]);
}

function commandSetOrgasmNumber(args) {
    setOrgasmNumber(args[0]);
}

function commandAddOrgasmNumber(args) {
    addOrgasmNumber(args[0]);
}

function commandShowUntilGraal(args) {
    showUntilGraal(args[0]);
}

function commandSetQuestGraalNumber(args) {
    setQuestGraalNumber(args[0]);
}

function isCommand(msg) {
    return (/^\//.test(msg));
}

function canAccessCommand(command, user) {
    if (command.accesser.includes(SCRIBE) && isScribe(user)) {
        return true;
    }
    if (command.accesser.includes(KNIGHT) && isKnight(user)) {
        return true;
    }
    if (command.accesser.includes(ROYAL_GUARD) && isRoyalGuard(user)) {
        return true;
    }
    if (command.accesser.includes(KQH) && isKqh(user)) {
        return true;
    }

    return false;
}

function processCommand(msg) {
    var match;
    var message = msg['m'];
    var user = msg['user'];

    //Bloc to just apply lowercase to command name, to keep param with caps
    var commandWitoutParam = message.split(' ')[0];
    message = message.replace(commandWitoutParam, commandWitoutParam.toLowerCase());

    for(var i = 0 ; i < commandsList.length ; i++){
        var command = commandsList[i];
        if (match = command.regex.exec(message)) {
            if(command.isMod && !(msg['is_mod'] || user === cb.room_slug)){
                cb.sendNotice('This commande is for model and mods only !', user);
            } else if (command.accesser && !(canAccessCommand(command, user) || msg['is_mod'] || user === cb.room_slug)) {
                cb.sendNotice('You don\'t have access to this command', user);
            } else {
                var args = [];
                for (var i = 1 ; match[i] ; i++) {
                    args.push(match[i]);
                }
                command.callback(args);
            }
            break;
        }
    }
}

grabSettings();
setCommands();
showKnightNotice();
