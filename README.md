Round Table Bot
===============


Round table bot is a charturbate bot inspired by an other bot (sorry, I don't have the name). It places the users around the round table !

----------


The grades
----------

- **Scribe** :  Not really around the round table but essential to keep notes up to date.
- **Knight** : Around the round table, they swear to defend those who can not do it by themselves.
- **Royal Guard** : Closer to their sovereign, they swear her/his protection.
- **King of the Queen's Heart** : Directly chosen by their queen. They are her most faithful companions.

The commands
------------

Round Table Bot provides model and mods with some commands to help :
- **`/makeScribe [username]`** : [username] become a Scribe.
- **`/makeKnight [username]`** : [username] become a Knight.
- **`/makeRoyalGuard [username]`** : [username] become a Royal Guard.
- **`/makeKQH [username]`** : [username] become a King of the Queen's Heart.
- **`/removeScribe [username]`** : [username] is no longer a Scribe.
- **`/removeKnight [username]`** : [username] is no longer a Knight.
- **`/removeRoyalGuard [username]`** : [username] is no longer a Royal Guard.
- **`/removeKQH [username]`** : [username] is no longer a King of the Queen's Heart.
- **`/setNickname [username] [nickename]`** : Set the nickname of [username] to [nickename].
- **`/removeNickname [username]`** : [username] has no longer a nickname.
- **`/setOrgasmNumber [number]`** : Set the current number of orgasm(s) to [number].
- **`/addOrgasm [number]`** : Add [number] orgasm(s) to the current number of orgasm(s).
- **`/showUntilGraal`** : Notice to all the number of orgasm(s) until the graal.
- **`/setQuestGraalNumber [number]`** : The total number of orgasm until the graal is set to [number].

Some commands are accessible for specific members :

Commands for **SCRIBE** :
- **`/setNickname [username] [nickename]`** : Set the nickname of [username] to [nickename].
- **`/removeNickname [username]`** : [username] has no longer a nickname.
- **`/setOrgasmNumber [number]`** : Set the current number of orgasm(s) to [number].
- **`/addOrgasm [number]`** : Add [number] orgasm(s) to the current number of orgasm(s).
- **`/showUntilGraal`** : Notice to all the number of orgasm(s) until the graal.

Commands for **KNIGHT** :
- **`/showUntilGraal`** : Notice to all the number of orgasm(s) until the graal.

Commands for **ROYAL GUARD** :
- **`/showUntilGraal`** : Notice to all the number of orgasm(s) until the graal.

Commands for **KING OF THE QUEEN'S HEART** :
- **`/showUntilGraal`** : Notice to all the number of orgasm(s) until the graal.